package test.java.com.junit.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
@DisplayName("I'm a Test Class")
public class DisplayNameTest {

	@DisplayName("I'm a Test method")
    @Test
    void test_spaces_ok() {
		boolean var=true;
		assertTrue(var);
    }

    @Test
    void test_spaces_fail() {
    	boolean var=true;
		assertFalse(!var);
    }

}