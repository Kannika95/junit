//package test.java.com.junit.services;
//
//import static org.junit.Assert.assertNotEquals;
//import static org.junit.Assert.assertTrue;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import main.java.com.junit.repository.ExampleInterFace;
//import main.java.com.junit.services.ExampleService;
//import main.java.com.junit.services.ExampleServiceImpl;
//
//@DisplayName("Im the Test Class")
//@SpringBootTest(classes =ExampleTest.class)
//public class ExampleTest {
//	
//	private static String a="";
//	private static boolean b=false;
//	
//	@Mock
//	private ExampleInterFace exampleInterFace;
//	
//	@InjectMocks
//	private ExampleService exampleService=new ExampleServiceImpl();
//	
//	@BeforeAll
//    public static void init() {
//    	a="Welcome";
//    	System.out.println(a);
//    }
//	
//	@BeforeEach
//	public void beforeEach() {
//		a=a+"Junit";
//		System.out.println(a);
//	}
//	
//	@DisplayName("Im the Test method")
//	@Test
//	public void getString() {
//		when(exampleInterFace.get()).thenReturn("Hello");
//		assertNotEquals(exampleService.get(), "Helloooo");
//	}
//
//	@Test
//	public void booleanCheck() {
//		assertTrue(b);
//	}
//
//
//	@AfterEach
//    public  void afterEach() {
//    	a="After Each is executing";
//    	System.out.println(a);
//    }
//	
//	@AfterAll
//    public static void afterAll() {
//    	a="After All is executing";
//    	System.out.println(a);
//    }
//}
