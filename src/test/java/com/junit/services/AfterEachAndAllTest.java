//package test.java.com.junit.services;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.RepeatedTest;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ContextConfiguration;
//
//import main.java.com.junit.StartApplication;
//import main.java.com.junit.repository.SampleRepository;
//import main.java.com.junit.services.SampleService;
//import main.java.com.junit.services.SampleServiceImpl;
//@ContextConfiguration(classes = StartApplication.class)
//@SpringBootTest
//public class AfterEachAndAllTest {
//
//	@Mock
//	private SampleRepository sampleRepository;
//
//	@InjectMocks // auto inject sampleRepository
//	private SampleService sampleService = new SampleServiceImpl();
//
//	@RepeatedTest(5)
//	public void addNumber() {
//		when(sampleRepository.retrieveAllData()).thenReturn(new int[] { 24, 15, 3 });
//		assertEquals(24, sampleService.findTheGreatestFromAllData());
//	}
//
//
//	@AfterEach
//	public void cleanUpEach(){
//		System.out.println("After Each cleanUpEach() method called");
//	}
//	
//	@AfterAll
//	public static void cleanUp(){
//		System.out.println("After All cleanUp() method called");
//	}
//
//}