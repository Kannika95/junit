//package test.java.com.junit.services;
//
//import static org.junit.Assert.assertNotEquals;
//import static org.junit.Assert.assertTrue;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.DynamicTest.dynamicTest;
//import static org.mockito.Mockito.when;
//
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.DynamicTest;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestFactory;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ContextConfiguration;
//
//import main.java.com.junit.StartApplication;
//import main.java.com.junit.repository.SampleRepository;
//import main.java.com.junit.services.SampleService;
//import main.java.com.junit.services.SampleServiceImpl;
//
///*@ContextConfiguration(classes = StartApplication.class)
//@SpringBootTest
//public class MockAndInjectTest  {
//
//    @Mock
//    private SampleRepository sampleRepository;
//
//    @InjectMocks // auto inject sampleRepository
//    private SampleService sampleService = new SampleServiceImpl();
//
//    @BeforeEach
//	void init_mocks() {
//		MockitoAnnotations.initMocks(this);
//	}
//    
//    @Test
//    public void assertEqual() {
//    	when(sampleRepository.get()).thenReturn("Hello JUnit 5");
//    	assertEquals(sampleService.get(), "Hello JUnit 6");
//    }
//    
//    @Test
//    public void assertNotEqual() {
//    	when(sampleRepository.get()).thenReturn("Hello JUnit 5");
//    	assertNotEquals(sampleService.get(), "Hello JUnit not matched");
//    }
//    
//}*/