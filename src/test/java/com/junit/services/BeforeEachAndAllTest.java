package test.java.com.junit.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;


public class BeforeEachAndAllTest {

	@BeforeAll
	public static void init(){
		System.out.println("BeforeAll init() method called");
	}

	@BeforeEach
	public void initEach(){
		System.out.println("BeforeEach initEach() method called");
	}
	@RepeatedTest(5)
	public void addNumber() {
		assertEquals(2, add(1,1));
	}

	public int add(int a, int b) {
		return a + b;
	}

}