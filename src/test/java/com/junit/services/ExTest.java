//package test.java.com.junit.services;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//import static org.mockito.Mockito.when;
//
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.RepeatedTest;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import main.java.com.junit.repository.ExampleRepo;
//import main.java.com.junit.services.Example;
//import main.java.com.junit.services.ExampleImpl;
//@SpringBootTest(classes=ExTest.class)
//public class ExTest {
//	public static long doubleArr(List<Long> arr,long b) {
//		Collections.sort(arr);
//		for(int index=0;index<arr.size();index++) {
//			if(arr.get(index)==b) {
//				b=b*2;
//			}
//		}
//		return b;
//		
//	}
//	
//	public static long playList(List<Integer> songs) {
//		long count=0;
//		Map<Integer,Integer> songsMap=new HashMap<Integer, Integer>();
//		for(int song:songs) {
//			if(songsMap.containsKey((60-song%60)%60)) {
//				count+=songsMap.get((60-song%60)%60);
//			}
//			songsMap.put(song%60, songsMap.getOrDefault(song%60, 0)+1);
//		}
//		return count;
//	}
//	class Point2D{
//		double x;
//		double y;
//		Point2D(double x,double y){
//			this.x=x;
//			this.y=y;
//		}
//		public double dist2D(Point2D p2) {
//			double d1=(p2.x-x)*(p2.x-x);
//			double d2=(p2.y-y)*(p2.y-y);
//			double d3=d1+d2;
//			return Math.sqrt(d3);
//			
//		}
//		public void printDistance(double d2) {
//			System.out.println("2D dist" + (int)Math.ceil(d2));
//		}
//	}
//	class Point3D extends Point2D{
//		double z;
//		public Point3D(double x,double y,double z){
//			super(x,y);
//			this.z=z;
//		}
//		public double dist23D(Point3D p2) {
//			double d1=(p2.x-x)*(p2.x-x);
//			double d2=(p2.y-y)*(p2.y-y);
//			double d3=(p2.y-y)*(p2.y-y);
//			double d4=d1+d2+d3;
//			return Math.sqrt(d4);
//			
//		}
//		public void printDistance(double d2) {
//			System.out.println("2D dist" + (int)Math.ceil(d2));
//		}
//	}
//	
//}
//class Result{
//	public static int jumps(int flagHeight,int bigjumps) {
//		return (flagHeight/bigjumps)   +   (flagHeight%bigjumps);
//	}
//}
