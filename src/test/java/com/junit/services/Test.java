package test.java.com.junit.services;

class Point3D extends Point2D{
 double z;
 public Point3D(double x, double y, double z){
 super(x,y);
 this.z = z;
 }
 public double dist3D(Point3D p3){
 double d1=(p3.x-x)*(p3.x-x);
 double d2 = (p3.y-y) * (p3.y-y);
 double d3 = (p3.z-z) * (p3.z-z);
 double d4 = d1+d2+d3;
 return Math.sqrt(d4);
 }
 public void printDistance(double d2){
 System.out.println("3D distance = " +(int)Math.ceil(d2));
 }
}
class Point2D{
 double x;
 double y;
 
 Point2D(double x,double y){
 this.x = x;
 this.y = y;
 }
 public double dist2D(Point2D p2){
	 double d1 = (p2.x-x)*(p2.x-x);
	 double d2 = (p2.y-y) * (p2.y-y);
	 double d3 = (d1+d2);
	 return Math.sqrt(d3);
	 }
	 public void printDistance(double d2){
	 System.out.println("2D distance = " +(int)Math.ceil(d2));
	 }
 }