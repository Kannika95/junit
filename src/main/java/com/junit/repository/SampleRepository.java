package main.java.com.junit.repository;

public interface SampleRepository {
	public String get();
	public int[] retrieveAllData();
	public String displayString();


//	public int getAddedValue(int a, int b);
}
