package main.java.com.junit.repository;

import org.springframework.stereotype.Repository;

@Repository
public class SampleRepositoryImpl implements SampleRepository {
    @Override
    public String get() {
        return "Hello JUnit 5";
    }

    public int[] retrieveAllData() {
		// Some dummy data
		// Actually this should talk to some database to get all the data
		return new int[] { 1, 2, 3, 4, 5 };
	}
//	@Override
//	public int getAddedValue(int a, int b) {
//		return a+b;
//	}

	@Override
	public String displayString() {
		return "Welcome to Junit";
	}

	
}
