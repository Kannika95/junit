package main.java.com.junit.repository;

import org.springframework.data.repository.CrudRepository;
import main.java.com.junit.model.LicUserEntity;

public interface LicRepository extends CrudRepository<LicUserEntity, Integer>{

}
