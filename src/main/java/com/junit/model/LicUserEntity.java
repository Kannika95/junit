package main.java.com.junit.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity  
@Table(name="lic_user_details")
public class LicUserEntity {
	
	@Id
	@Column
	private int userId;
	
	@Column
	private String userName;
	
	@Column
	private String password;
	
	@Column
	private String address;
	
	@Column
	private String contact;
	
	@Column
	private String email;
	
	public void setUserId(int userId) {
		this.userId=userId;
	}
	
	public void setUserName(String userName) {
		this.userName=userName;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}
	
	public void setAddress(String address) {
		this.address=address;
	}
	
	public void setContact(String contact) {
		this.contact=contact;
	}
	
	public void setEmail(String email) {
		this.email=email;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getContact() {
		return contact;
	}
	
	public String getEmail() {
		return email;
	}
	

}
