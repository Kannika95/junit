package main.java.com.junit.controller;

import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SampleController {

//   @ResponseBody
//    @GetMapping("/get")
//    public String hello(HttpServletRequest request) {
//        return "https://junit.org/junit5/docs/current/user-guide/";
//    }
	
	@RequestMapping("/get")
	public ResponseEntity<Object> redirectToExternalUrl() throws URISyntaxException {
	    URI yahoo = new URI("https://junit.org/junit5/docs/current/user-guide/");
	    HttpHeaders httpHeaders = new HttpHeaders();
	    httpHeaders.setLocation(yahoo);
	    return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
	}

}