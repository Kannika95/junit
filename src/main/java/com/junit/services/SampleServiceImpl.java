package main.java.com.junit.services;

import main.java.com.junit.repository.SampleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleServiceImpl implements SampleService {

    @Autowired
    SampleRepository sampleRepository;

    @Override
    public String get() {
        return sampleRepository.get();
    }
    
    public int findTheGreatestFromAllData() {
		int[] data = sampleRepository.retrieveAllData();
		int greatest = Integer.MIN_VALUE;

		for (int value : data) {
			if (value > greatest) {
				greatest = value;
			}
		}
		return greatest;
	}

	@Override
	public String displayString() {
		return sampleRepository.displayString();
	}
	@Override
	public int addition(int a, int b) {
		return a+b;
		
	}

//	@Override
//	public int getAddedValue(int a, int b) {
//		return sampleRepository.getAddedValue(a, b);
//	}



}
