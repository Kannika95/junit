package main.java.com.junit.services;

public interface SampleService {

    String get();

	int findTheGreatestFromAllData();

	String displayString();

	int addition(int i, int j);

//	int getAddedValue(int a,int b);

}
