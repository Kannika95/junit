package main.java.com.junit.services;

import org.springframework.stereotype.Service;

import main.java.com.junit.repository.ExampleInterFace;

@Service
public class ExampleServiceImpl implements ExampleService{
	
	private ExampleInterFace exampleInterFace;

	@Override
	public String get() {
		// TODO Auto-generated method stub
		return exampleInterFace.get();
	}

}
