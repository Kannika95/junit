package main.java.com.junit.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import main.java.com.junit.model.LicUserEntity;
import main.java.com.junit.repository.LicRepository;

@Service
public class LicService {

	@Autowired
	 LicRepository licRepository;  

	public void saveData(LicUserEntity licUserEntity) {
		licRepository.save(licUserEntity);
	}
	
}
